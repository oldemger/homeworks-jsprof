document.addEventListener('DOMContentLoaded', () =>{


	let Zoo = {
        name: 'YouZoo',
        AnimalCount: 0,
        zones: {
            mammals: [],
            birds: [],
            fishes: [],
            reptiles: [],
            others: []
        },
        addAnimal: function(animalObj){
            switch (animalObj.zone){
                case "mammal":
                    Zoo.zones.mammals.push(animalObj);
                    break;
                case "bird":
                    Zoo.zones.birds.push(animalObj);
                    break;
                case "fish":
                    Zoo.zones.fishes.push(animalObj);
                    break;
                case "reptile":
                    Zoo.zones.reptiles.push(animalObj);
                    break;
                default: 
                    Zoo.zones.others.push(animalObj);
            }
            console.log(Zoo.zones);
        },
        removeAnimal: function(animalName){
            for (key in Zoo.zones){
                let i = 0,
                arr = Zoo.zones[key];
                for ( i; i < arr.length; i++ ){
                    if(arr[i].name === animalName){
                        arr.splice(i, 1);
                    }
                }
            }
        },
        getAnimal: function(type, value){
            let typeAnimals = [];
            for (key in Zoo.zones){
                let j = 0,
                arr = Zoo.zones[key];
                switch (type){
                    case "name":
                        for ( j; j < arr.length; j++ ){
                            if(arr[j].name === value){
                                console.log(arr[j]);
                            }
                        }
                        break;
                    case "type":
                        for ( j; j < arr.length; j++ ){
                            if(arr[j].type === value){
                                typeAnimals.push(arr[j]);                
                            }
                        }  
                        break;   
                    default:
                        console.log("We haven't this animal");
                }
            }
            console.log(typeAnimals);
        },
        countAnimals: function(){
            
            for (key in Zoo.zones){
                Zoo.AnimalCount += Zoo.zones[key].length;
            }
            console.log("There are " + Zoo.AnimalCount + " animals in Zoo");
        }
}


class Animal {
  constructor(name, phrase, foodType) {
    this.name = name;
	this.phrase = phrase;
	this.foodType = foodType;
  }
	eatSomething() {
		alert("I eat: " + this.foodType);
	}
}


class Mammals extends Animal{
	constructor(name, phrase, foodType, type, speed ){
		super(name, phrase, foodType);
    	this.type = type; 
    	this.speed = speed;
    	this.zone = "mammal";
  	}
	     run(){
	        console.log( this.name + " run with speed " +  this.speed + " km/h" );
	    }
}

class Birds extends Animal{
	constructor(name, phrase, foodType, type, speed ){
		super(name, phrase, foodType);
    	this.type = type;
    	this.speed = speed;
    	this.zone = "bird";
  	}
	     fly(){
	        console.log( this.name + " fly with speed " +  this.speed + " km/h" );
	    }
}

class Fishes extends Animal{
	constructor(name, phrase, foodType, type, speed){
		super(name, phrase, foodType);
    	this.type = type;
    	this.speed = speed;
    	this.zone = "fish";
  	}
        swim(){
	        console.log( this.name + " swim with speed " +  this.speed + " km/h" );
	    }
}
class Reptiles extends Animal{
	constructor(name, phrase, foodType, type, speed){
		super(name, phrase, foodType);
    	this.type = type;
    	this.speed = speed;
    	this.zone = "reptile";
  	}
        crawl(){
	        console.log( this.name + " crawl with speed " +  this.speed + " km/h" );
	    }
}

let dog = new Mammals("Sharik", "gav", "carnivore", "dog", 20);
// dog.run();
// console.log(dog);
Zoo.addAnimal(dog);

let ara = new Birds("Ara", "chirrup", "herbivore", "parrot", 30);
// ara.fly();
// console.log(ara);
Zoo.addAnimal(ara);

let martlet = new Birds("Lina", "chirrup", "herbivore", "martlet", 40);
Zoo.addAnimal(martlet);

let pike = new Fishes("Lucia", "nothing", "carnivore", "pike", 10);
// pike.swim();
// console.log(pike);
Zoo.addAnimal(pike);

let anaconda = new Reptiles("Manya", "shhhh", "carnivore", "snake", 15);
// anaconda.crawl();
// console.log(anaconda);
Zoo.addAnimal(anaconda);
let cobra = new Reptiles("Ket", "shhhh", "carnivore", "snake", 18);
Zoo.addAnimal(cobra);

Zoo.getAnimal("type", "snake");
Zoo.getAnimal("name", "Lucia");

Zoo.countAnimals();
Zoo.removeAnimal("Ara");
console.log(Zoo.zones);
})
