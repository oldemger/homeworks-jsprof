/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/firstClasswork.js":
/*!***************************************!*\
  !*** ./application/firstClasswork.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Post = function Post(_ref) {\n  var id = _ref.id,\n      link = _ref.link,\n      name = _ref.name,\n      description = _ref.description,\n      image = _ref.image,\n      _ref$like = _ref.like,\n      like = _ref$like === void 0 ? 0 : _ref$like;\n\n  _classCallCheck(this, Post);\n\n  this.id = id;\n  this.link = link;\n  this.name = name;\n  this.description = description;\n  this.image = image;\n  this.like = like;\n};\n\nvar data = [{\n  id: 1,\n  link: \"#1\",\n  name: \"Established fact123123\",\n  description: \"The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. \",\n  image: \"http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg\"\n}, {\n  id: 2,\n  link: \"#2\",\n  name: \"Established fact\",\n  description: \"The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. \",\n  image: \"http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg\"\n}, {\n  id: 6,\n  link: \"#6\",\n  name: \"Many packages\",\n  description: \"Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.\",\n  image: \"http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg\"\n}, {\n  id: 3,\n  link: \"#3\",\n  name: \"Suffered alteration\",\n  description: \"Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature.\",\n  image: \"http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg\"\n}, {\n  id: 4,\n  link: \"#4\",\n  name: \"Discovered source\",\n  description: \"It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged\",\n  image: \"http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg\"\n}, {\n  id: 5,\n  link: \"#5\",\n  name: \"Handful model\",\n  description: \"The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\",\n  image: \"http://telegram.org.ru/uploads/posts/2017-10/1507400876_file_162328.jpg\"\n}];\ndocument.addEventListener('DOMContentLoaded', function () {\n  var post1 = new Post(3, \"#2\", \"lorem ipsum\", \"elfk;e ;oekoeo okokodwpef lsd laksmdl lksmedlk\", \"http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg\");\n  console.log(post1);\n});\n\n//# sourceURL=webpack:///./application/firstClasswork.js?");

/***/ }),

/***/ "./application/firstHomework.js":
/*!**************************************!*\
  !*** ./application/firstHomework.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\ndocument.addEventListener('DOMContentLoaded', function () {\n  var Zoo = {\n    name: 'YouZoo',\n    AnimalCount: 0,\n    zones: {\n      mammals: [],\n      birds: [],\n      fishes: [],\n      reptiles: [],\n      others: []\n    },\n    addAnimal: function addAnimal(animalObj) {\n      switch (animalObj.zone) {\n        case \"mammal\":\n          Zoo.zones.mammals.push(animalObj);\n          break;\n\n        case \"bird\":\n          Zoo.zones.birds.push(animalObj);\n          break;\n\n        case \"fish\":\n          Zoo.zones.fishes.push(animalObj);\n          break;\n\n        case \"reptile\":\n          Zoo.zones.reptiles.push(animalObj);\n          break;\n\n        default:\n          Zoo.zones.others.push(animalObj);\n      }\n\n      console.log(Zoo.zones);\n    },\n    removeAnimal: function removeAnimal(animalName) {\n      for (key in Zoo.zones) {\n        var i = 0,\n            arr = Zoo.zones[key];\n\n        for (i; i < arr.length; i++) {\n          if (arr[i].name === animalName) {\n            arr.splice(i, 1);\n          }\n        }\n      }\n    },\n    getAnimal: function getAnimal(type, value) {\n      var typeAnimals = [];\n\n      for (key in Zoo.zones) {\n        var j = 0,\n            arr = Zoo.zones[key];\n\n        switch (type) {\n          case \"name\":\n            for (j; j < arr.length; j++) {\n              if (arr[j].name === value) {\n                console.log(arr[j]);\n              }\n            }\n\n            break;\n\n          case \"type\":\n            for (j; j < arr.length; j++) {\n              if (arr[j].type === value) {\n                typeAnimals.push(arr[j]);\n              }\n            }\n\n            break;\n\n          default:\n            console.log(\"We haven't this animal\");\n        }\n      }\n\n      console.log(typeAnimals);\n    },\n    countAnimals: function countAnimals() {\n      for (key in Zoo.zones) {\n        Zoo.AnimalCount += Zoo.zones[key].length;\n      }\n\n      console.log(\"There are \" + Zoo.AnimalCount + \" animals in Zoo\");\n    }\n  };\n\n  var Animal =\n  /*#__PURE__*/\n  function () {\n    function Animal(name, phrase, foodType) {\n      _classCallCheck(this, Animal);\n\n      this.name = name;\n      this.phrase = phrase;\n      this.foodType = foodType;\n    }\n\n    _createClass(Animal, [{\n      key: \"eatSomething\",\n      value: function eatSomething() {\n        alert(\"I eat: \" + this.foodType);\n      }\n    }]);\n\n    return Animal;\n  }();\n\n  var Mammals =\n  /*#__PURE__*/\n  function (_Animal) {\n    _inherits(Mammals, _Animal);\n\n    function Mammals(name, phrase, foodType, type, speed) {\n      var _this;\n\n      _classCallCheck(this, Mammals);\n\n      _this = _possibleConstructorReturn(this, _getPrototypeOf(Mammals).call(this, name, phrase, foodType));\n      _this.type = type;\n      _this.speed = speed;\n      _this.zone = \"mammal\";\n      return _this;\n    }\n\n    _createClass(Mammals, [{\n      key: \"run\",\n      value: function run() {\n        console.log(this.name + \" run with speed \" + this.speed + \" km/h\");\n      }\n    }]);\n\n    return Mammals;\n  }(Animal);\n\n  var Birds =\n  /*#__PURE__*/\n  function (_Animal2) {\n    _inherits(Birds, _Animal2);\n\n    function Birds(name, phrase, foodType, type, speed) {\n      var _this2;\n\n      _classCallCheck(this, Birds);\n\n      _this2 = _possibleConstructorReturn(this, _getPrototypeOf(Birds).call(this, name, phrase, foodType));\n      _this2.type = type;\n      _this2.speed = speed;\n      _this2.zone = \"bird\";\n      return _this2;\n    }\n\n    _createClass(Birds, [{\n      key: \"fly\",\n      value: function fly() {\n        console.log(this.name + \" fly with speed \" + this.speed + \" km/h\");\n      }\n    }]);\n\n    return Birds;\n  }(Animal);\n\n  var Fishes =\n  /*#__PURE__*/\n  function (_Animal3) {\n    _inherits(Fishes, _Animal3);\n\n    function Fishes(name, phrase, foodType, type, speed) {\n      var _this3;\n\n      _classCallCheck(this, Fishes);\n\n      _this3 = _possibleConstructorReturn(this, _getPrototypeOf(Fishes).call(this, name, phrase, foodType));\n      _this3.type = type;\n      _this3.speed = speed;\n      _this3.zone = \"fish\";\n      return _this3;\n    }\n\n    _createClass(Fishes, [{\n      key: \"swim\",\n      value: function swim() {\n        console.log(this.name + \" swim with speed \" + this.speed + \" km/h\");\n      }\n    }]);\n\n    return Fishes;\n  }(Animal);\n\n  var Reptiles =\n  /*#__PURE__*/\n  function (_Animal4) {\n    _inherits(Reptiles, _Animal4);\n\n    function Reptiles(name, phrase, foodType, type, speed) {\n      var _this4;\n\n      _classCallCheck(this, Reptiles);\n\n      _this4 = _possibleConstructorReturn(this, _getPrototypeOf(Reptiles).call(this, name, phrase, foodType));\n      _this4.type = type;\n      _this4.speed = speed;\n      _this4.zone = \"reptile\";\n      return _this4;\n    }\n\n    _createClass(Reptiles, [{\n      key: \"crawl\",\n      value: function crawl() {\n        console.log(this.name + \" crawl with speed \" + this.speed + \" km/h\");\n      }\n    }]);\n\n    return Reptiles;\n  }(Animal);\n\n  var dog = new Mammals(\"Sharik\", \"gav\", \"carnivore\", \"dog\", 20); // dog.run();\n  // console.log(dog);\n\n  Zoo.addAnimal(dog);\n  var ara = new Birds(\"Ara\", \"chirrup\", \"herbivore\", \"parrot\", 30); // ara.fly();\n  // console.log(ara);\n\n  Zoo.addAnimal(ara);\n  var martlet = new Birds(\"Lina\", \"chirrup\", \"herbivore\", \"martlet\", 40);\n  Zoo.addAnimal(martlet);\n  var pike = new Fishes(\"Lucia\", \"nothing\", \"carnivore\", \"pike\", 10); // pike.swim();\n  // console.log(pike);\n\n  Zoo.addAnimal(pike);\n  var anaconda = new Reptiles(\"Manya\", \"shhhh\", \"carnivore\", \"snake\", 15); // anaconda.crawl();\n  // console.log(anaconda);\n\n  Zoo.addAnimal(anaconda);\n  var cobra = new Reptiles(\"Ket\", \"shhhh\", \"carnivore\", \"snake\", 18);\n  Zoo.addAnimal(cobra);\n  Zoo.getAnimal(\"type\", \"snake\");\n  Zoo.getAnimal(\"name\", \"Lucia\");\n  Zoo.countAnimals();\n  Zoo.removeAnimal(\"Ara\");\n  console.log(Zoo.zones);\n});\n\n//# sourceURL=webpack:///./application/firstHomework.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _firstHomework_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./firstHomework.js */ \"./application/firstHomework.js\");\n/* harmony import */ var _firstHomework_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_firstHomework_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _firstClasswork_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firstClasswork.js */ \"./application/firstClasswork.js\");\n/* harmony import */ var _firstClasswork_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_firstClasswork_js__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });